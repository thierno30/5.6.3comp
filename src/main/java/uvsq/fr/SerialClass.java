package uvsq.fr;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerialClass {
        
	public static void serialiser (List<Forme> form) {
		ObjectOutputStream oos = null ; 
		FileOutputStream out = null  ; 

		try {
			out = new FileOutputStream("MesFormes.txt");
			oos = new ObjectOutputStream(out);
			oos.writeObject(form);
		}
		catch(IOException io) {
			io.printStackTrace();
		}
		finally {
			try {
				out.close();
				oos.close();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
	public static List<Forme> deserialiser() {
		ObjectInputStream ois = null; 
		FileInputStream in = null; 
		List<Forme> form = null;

		try {
			in = new FileInputStream("MesFormes.txt");
			
			ois = new ObjectInputStream(in);
			
			form = (List<Forme>) ois.readObject();
		}
		catch(IOException io) {
			io.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		finally {
			try {
				in.close();
				ois.close();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return form;
	}
}
