package uvsq.fr;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Fen1 extends JFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Manip manip = new Manip();
	
	private JButton b2 =new JButton ("reinitialiser");
	   private JButton b3 =new JButton ("chargement");
	   private JButton b4  =new JButton ("sauvegarde");
	   
	public Fen1() {
		 manip.add(b2);  manip.add(b3);  manip.add(b4); 
		 b2.addActionListener(this);
		 b3.addActionListener(this);
		 b4.addActionListener(this);
	       this.setTitle("Fenetre de dessin2D");
	       this.setSize(600,400);
	       this.setLocationRelativeTo(null);
	       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	       this.getContentPane().add(manip);
	       manip.setFocusable(true);
	       manip.requestFocus();
	       this.setVisible(true);
	       //manip.deplace();
	       
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == b4) {
			manip.sauve();
		}
		if(arg0.getSource() == b3) {
			manip.charge();
		}
		if(arg0.getSource() == b2) {
			manip.reinitialiser();
		}
	}
	

	
}
