package uvsq.fr;



public abstract class Forme {
     private int x;
     private int y;
     private int l;
     private int h;
	
	public Forme(int x, int y, int l, int h) {
		super();
		this.x = x;
		this.y = y;
		this.l = l;
		this.h = h;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getL() {
		return l;
	}

	public void setL(int l) {
		this.l = l;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}
     
	public boolean contains (int a,int b) {
		int extX,extY; 
		extX=x+50 ;
		extY=y+50 ;
		if (x<a && a<extX && y<b && b<extY) return true;
		else return false;
	}
     
}
